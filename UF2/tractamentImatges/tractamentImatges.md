# Tractament d'imatges

### Exercici 1

Partint de les imatges que hi ha a l'arxiu comprimit adjunt (Exercici1.zip), realitza les conversions més adequades per poder-los publicar a un web. Tingues en compte que has de trobar un balanç entre l'aparença visual i la mida del fitxer. Digues, de forma justificada, quines opcions has escollit per als següents paràmetres de destí:

Imatge 1:

* Format del fitxer: jpg
* Profunditat de color: 24 bits
* Tipus o qualitat de compressió: "90"
* Mida (en kB) del fitxer: 26000kb

Al ser una fotografia lo més recomenable es convertirlo en format ```jpg```. Llavors la profunditat de color seria de 24 bits. El tipus de compressió que utilitzariem es el format TIFF, que ens permet editar i manipular imatges, ja que si treballem directament amb el format jpg, s'acumularan errors cada vegada que grabem l'imatge.

Imatge 2:

* Format del fitxer: png
* Profunditat de color: 24 bits
* Tipus o qualitat de compressió: "9"
* Mida (en kB) del fitxer: 3.0kb

Estem treballant amb logos així que lo millor es treballar amb png que ens ajudaran a no perdre qualitat a l'hora de modificar el tamany de la pantalla. La profunditat de color es de 16 bits.

Imatge 3:

* Format del fitxer: png
* Profunditat de color: 8 bits
* Tipus o qualitat de compressió: "9"
* Mida (en kB) del fitxer: 2.6kb

Al igual que l'exemple anterior, utilitzem logos i el format recomenable es un vectorial com svg o eps. La profunditat de color es de 8 bits, ja que al ser blanc i negre, no necesita gran quantitat de colors.

### Exercici 2

Partint del fitxer adjunt (Exercici2.png), que és un PNG. Podries convertir-lo a un format vectorial (SVG o EPS)?

Es pot convertir a SVG, pero l'imatge perd molta qualitat, i no volem que si la mida de finestra de la web es molt gran, veure el logo de forma borrosa.

Utilitza la pàgina http://logosvg.com/ per a descarregar-te el logotip en format SVG que prefereixis. Obre amb un editor de textos el fitxer SVG i observa'n el contingut XML. Quina diferència principal hi ha amb un fitxer PNG? En aquesta ocasió, podries fer la conversió d'SVG a PNG?


***Resposta***: La principal diferencia entre SVG y PNG es que SVG es pot animar i escalar a gran tamany sense perdre qualitat. Altra diferencia es que amb SVG, com a disenyador, pot canviar els colors molt facilment perque te capes.

En canvi, SVG no es el format adequat per a fotografies, ja que PNG sol tenir una millor resolució.

En aquesta ocasió si es pot fer la conversió a PNG.

### Exercici 3

A l'arxiu comprimit adjunt (Exercici3.zip) hi tens 3 imatges que volem publicar a un web en un carroussel de bootstrap. Fes les modificacions necessàries (de format, mida del fitxer i amplada/llargada de la imatge) perquè el carroussel es visualitzi correctament a un web. La mida del carroussel la podeu decidir vosaltres.

En concret, cal que retalleu ("crop", en anglès) totes les imatges per tal que tinguin la mateixa amplada i llargada en píxels per tal que es visualitzin correctament. Demostra el seu correcte funcionament incrustant el carroussel amb les imatges en una pàgina.

Incloeu un enllaç a la URL de la pàgina HTTP o GitLab que contingui aquest carroussel.

Link gitlab: 
https://gitlab.com/iaw47871833/m09-disseny_interficies_web/-/blob/master/UF2/tractamentImatges/Exercici3/
