### Pagina principal

https://demo.themewagon.com/preview/free-responsive-bootstrap-4-html5-business-website-template-humanresources

1. Modificar botones de acceso (alumno, profesor, empresa)
2. https://startbootstrap.com/previews/landing-page/ (Copiar el buscador de esta plantilla)

Hem escolit aquesta plantilla perquè els elements principals com el nav i el logo són la base de la pàgina web i ens servirà per tots el wireframes. L'únic que ens caldrà afegir per completarla és un buscador.  
A més modificarem les cartes del bootstrap que ens serviràn com a accés als diferents tipus d'usuari (alumne, professor, empresa)

### Pagina de login

https://demo.themewagon.com/preview/free-responsive-bootstrap-4-html5-education-website-template-studylab

### Pagina inicio (Alumno, profesor, empresa)

https://demo.themewagon.com/preview/free-responsive-bootstrap-4-html5-educational-website-template-tutor

1. Añadir buscador