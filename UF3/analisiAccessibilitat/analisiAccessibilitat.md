# Activitat 2.1. Anàlisi d'accessibilitat

Anàlisi de l'accessibilitat d'una pàgina web pública

Escolliu una pàgina web pública (d'un ministeri o ajuntament, per exemple) i feu-ne un anàlisi complet de l'accessibilitat:

Primer, analitzeu manualment tots i cadascun dels criteris descrits a Comprovacions bàsiques d'accessibilitat. Incloeu el resultat de l'anàlisi, tant si ha sigut favorable com si heu trobat disconfirmitats

Després, feu-ne un anàlisi automàtic amb un validador (TAW, per exemple). Analitzeu les disconfirmitats i les advertències que detecti el validador, indicant quin és el vostre diagnòstic

Per a cada criteri analitzat digueu:

Mostreu el codi de la línia de codi HTML afectada
Quin és el vostre diagnòstic, és a dir, si considereu que realment és una disconfirmitat o no i la justificació

En cas que sigui una disconformitat, proposeu la mesura correctiva oportuna

La pàgina a analitzar és la de l'ajuntament de Barcelona

https://ajuntament.barcelona.cat/es/

### Titols

Els titols son importants per poder orientar a l'usuari e indicar on es troben mentre canvien de pàgines. El primer que veu l'usuari quan va a una pàgina web és el títol.

En aquest cas trobem titols descriuen correctament en quina secció de l'aplicació es troba.

```
<title>Bienvenido a Ayuntamiento de Barcelona | Ayuntamiento de Barcelona</title>
<title>Seguridad y Prevención | Ajuntament de Barcelona</title>
<title>Oferta pública de empleo y convocatorias de trabajo | Sede electrónica | Ajuntament de Barcelona</title>
```

![titles](./img/titles.png)

Com podem comprobar el titol es diferent en diferents pàgines de la web i distingueix adecuadament una pàgina de les altres.

Una cosa bona fan bé en aquesta web es posar primer l'informació principal de la pàgina.

Posem per exemple la ```pàgina de seguridad i prevención de l'ajuntament de barcelona```.

El titol és: ```Seguridad y Prevención | Ajuntament de Barcelona```.

Com podem veure, mostra l'informació important al principi i no el nom de la web.

Aixó és positiu perqué si per exemple tenim varies págines en diferents pestanyes i mostrem el nom de la web primer, no sabriem en quina secció ens trobem.

### Text alternatius en les imatges

