# Activitat 1.1. Anàlisi d'usabilitat sense usuaris

Feu un informe en PDF de l'avaluació sense usuaris, amb un apartat per cada exercici. Es tracta de millorar el producte i no de vendre que ja és correcte: com més punts de millora detecteu, proposant una alternativa de disseny concreta que ho solucioni, millor valoració tindreu.

### Exercici 1

#### Defineix una tasca que es podria fer a la teva pàgina web, amb totes les accions que implicaria. Ha d'involucrar com a mínim 2 pantalles diferents i unes 5 accions en total. És molt important escollir una bona tasca, explicar-la bé, incloent les accions implicades. ***No són tasques vàlides accions comunes a qualsevol pàgina com el registre o l'autenticació d'un usuari***.

Acció: Enviar una oferta a un alumne

Aquesta acció consisteix en que a partir de l'usuari "Professor", aquest envia una oferta de treball a un dels seus alumnes. Les accions que tindra que seguir el professor seran:

1. Loguejarse com professor.
2. Buscar ofertes, segons les necessitats de l'alumnat.
3. Si troba alguna oferta interessant, li donara al botó enviar.

![IniciProfessor](./img/iniciProfessor.png)

4. Visitar perfil alumne si es necessari.

5. A continuació es mostrara un llistat dels seus alumne, on escullira a quin alumne vol enviar l'oferta.

![LlistatAlumnes](./img/alumnes.png)

### Exercici 2

#### Partint de les 10 regles heurístiques de Nielsen, executa de forma anàlitica la tasca anterior. Per a cadascuna de les regles heurístiques, digues quins punts de millora has detectat.

1. Visibilidad del estado del sistema

En el moment que el professor envia l'oferta a l'alumne, es tindra que enviar un missatge a l'usuari si l'oferta s'ha enviat correctament. O en un cas possible, mostrar un missatge si l'oferta ja no esta disponible.

2. Relación entre el sistema y el mundo real

S'utilitzara un llenguatge proper a l'usuari, en comptes del llenguatge anglés i es controlara que no es mostrin missatges relacionats amb el sistema i mostrar en comptes missatges personalitzats.

3. Control y libertad del usuario

Si l'usuari esta a punt d'enviar una oferta pero vol buscar un altre oferta pot anar a la llista d'ofertes a partir de la nav.

El que es podria millorar es posar alguna opció de navegacio a la llista d'alumnes a qui vol enviar les ofertes, ja que sino només es podra arribar a aquesta vista només si li donem al botó d'enviar d'una oferta.

4. Consistencia y estándares

Pot haber la casualitat que l'usuari es confongui amb la vista dels alumnes a qui pot enviar les ofertes i la vista de "Mis alumnos".

5. Prevención de errores

S'haurà de treballar en els possibles errors que l'usuari podria sufrir i evitar-les.

6. Reconocimiento antes que recuerdo

La llista d'ofertes es tendrien que guardar en algun tipus de sessió en el cas de que l'usuari vulgui retornar en un altre moment.

7. Flexibilidad y eficiencia de uso

8. Estética y diseño minimalista

Utilitzar dialegs rellevants que tot tipus d'usuari pugui entendre

9. Ayudar a los usuarios a reconocer

En el cas de que hi hagi error, indicar clarament quin es el problema i com solucionar-ho.

10. Ayuda y documentación:

En les accions on hi pot haber problemes, es pot posar HINTs o pistes, per ajudar a l'usuari i indicar quina acció ha de fer.

### Exercici 3

#### Amb la tasca definida a l'exercici 1, realitzeu un passeig cognitiu sobre la vostra aplicació. Es tracta que, per cada una de les accions detectades, respongueu a aquestes preguntes:

a) Els usuaris intentaran realitzar el seu objectiu de forma correcta?

Faran lo possible per poder enviar l'oferta a l'alumne correctament.

b) Serà visible per als usuaris l'acció a realitzar?

Només si l'usuari clica al botó d'enviar oferta en la llista d'ofertes.

c) Entendran que l'acció que han de fer (la de el punt anterior) és la correcta?

S'indicarà amb un HINT les diferents accions a realitzar.

d) Se n'adonarà l'usuari que està avançant cap a la solució de la tasca?

Ho sabrà quan finalment a enviat l'oferta i es mostra un missatge de confirmació.

### Exercici 4

#### Proposeu almenys 4  millores al disseny de la vostra pàgina en base al que hagueu detectat als exercicis anteriors. Cal descriure la millora i acompanyar-la d'una captura de pantalla o un wireframe (si el canvi és més gran)

1. Afegir algun tipus d'opció com una checkbox, per poder enviar ofertes a més d'un alumne a la vegada, per a que el professor no tingui que enviar l'oferta un per un.

![EnviarOferta](./img/enviarOferta.png)

2. Mostrar petites finestres d'ajuda a l'usuari per saber quina acció a seguir. A més d'alertes

![Hints](./img/milloraHint1.png)

3. Guardar en una sessió les ofertes seleccionades per el professor, per no tenir que buscarles un altre vegada en cas de tancar el navegador o anar a una altre vista de la pàgina web.

4. Tenir alguna opció en la nav per anar directament a la vista d'ofertes guardades per enviar.

![OpcioGuardades](./img/opcioGuardades.png)